// DATA MODELS
const RequestModel = require('../models/request')
const shortid = require('shortid')
const moment = require('moment')

// MOCK DATA
const mocks = [
  {
    status: 'En Trámite',
    comments: [
      {
        fullname: 'Satya Nadella',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-24'),
        status: 'En Revisión'
      },
      {
        fullname: 'Bill Gates',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-25'),
        status: 'En Revisión'
      },
      {
        fullname: 'Elon Musk',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-26'),
        status: 'Negado'
      },
      {
        fullname: 'Sergei Brin',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-27'),
        status: 'En Trámite'
      }
    ]
  },
  {
    status: 'Completado',
    comments: [
      {
        fullname: 'Barack Obama',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-21'),
        status: 'En Revisión'
      },
      {
        fullname: 'Phil Harris',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-23'),
        status: 'Completado'
      },
    ]
  },
  {
    status: 'Completado',
    comments: [
      {
        fullname: 'Barack Obama',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-21'),
        status: 'En Revisión'
      },
      {
        fullname: 'Barack Obama',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-22'),
        status: 'En Revisión'
      },
      {
        fullname: 'Phil Harris',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-23'),
        status: 'Completado'
      },
    ]
  },
  {
    status: 'Completado',
    comments: [
      {
        fullname: 'Phil Harris',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-23'),
        status: 'Completado'
      },
    ]
  },
  {
    status: 'Abierto',
    comments: [
      {
        fullname: 'Phil Harris',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-23'),
        status: 'Abierto'
      },
    ]
  },
  {
    status: 'Abierto',
    comments: [
      {
        fullname: 'Elon Musk',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada aliquam felis, a porttitor eros aliquam quis. Duis leo tellus, vestibulum id odio non, finibus',
        date: new Date('2021-06-25'),
        status: 'Abierto'
      },
    ]
  }
]

// CATEGORY CONTROLLER
module.exports = {
  CreateRequest: (req, res, next) => {
    const random = Math.floor(Math.random()*(6-0+1)+0);
    const pin = ('0' + Math.floor(Math.random() * (9999 - 0 + 1)) + 0).substr(-4)
    const NewRequest = new RequestModel({
      createdDate: new Date(),
      pinDate: new Date(),
      consecutive: shortid.generate(),
      pin,
      ...req.body,
      ...mocks[random]
    })
    NewRequest.save((error, request) => {
      if (error) {
        return next(error)
      } else {
        return res.status(200).send(request)
      }
    })
  },
  FetchRequest: (req, res, next) => {
    const query = RequestModel.findOne({
      $and: [
        {
          pin: req.query.pin
        },
        {
          $or: [
            {
              email: req.query.email
            },
            {
              cellphone: req.query.cellphone
            }
          ]
        }
      ]
    })
    query.exec((error, request) => {
      if (error) {
        return next(error)
      } else if (request) {
        const now = moment()
        const pinDate = moment(request.pinDate)
        const difference = now.diff(pinDate, 'minutes')
        if (difference < 10) {
          res.status(200).send([request])
        } else {
          res.status(403).end()
        }
      } else {
        res.status(200).send([])
      }
    })
  },
  RestorePIN: (req, res, next) => {
    const query = RequestModel.findOne({
      $or: [
        {
          email: req.body.email
        },
        {
          cellphone: req.body.cellphone
        }
      ]
    })
    query.exec((error, request) => {
      if (error) {
        return next(error)
      } else {
        const pin = ('0' + Math.floor(Math.random() * (9999 - 0 + 1)) + 0).substr(-4)
        request.pin = pin
        request.pinDate = new Date()
        request.save((error) => {
          if (error) {
            return next(error)
          } else {
            res.status(200).send(pin)
          }
        })
      }
    })
  },
  FileUploaderCallback: (req, res, next) => {
    res.status(200).send(req.file.filename)
  }
}
