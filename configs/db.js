// DATABASE SETTIGS
module.exports = {
  url: 'mongodb://127.0.0.1:27017/evolution',
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
}
