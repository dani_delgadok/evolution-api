// IMPORT LIBRARIES
const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
const cors = require('cors')
const helmet = require('helmet')
const path = require('path')

// IMPORT CONFIGS
const db = require('./configs/db')

// MONGOOSE SETTINGS
mongoose.connect(db.url, db.options)

// SERVER SETTINGS
const port = 3002
const app = express()

// EXPRESS SETTINGS
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(helmet())
app.use(cors())
app.use(morgan('dev'))

// EXPRESS STATIC FILES
app.use('/api/file', express.static(path.resolve(__dirname, '/applications/uploads')))

// EXPRESS ROUTES
require('./routes/request')(app)

// RUN SERVER
app.listen(port, () => {
  console.log('Server is now running at port: ', port)
})
