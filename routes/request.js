// LIBRARIES
const multer = require('multer')

// CONTROLLERS
const RequestController = require('../controllers/request')
const CommonController = require('../controllers/common')

// MULTER SETTINGS
const uploader = multer({ dest: '/applications/uploads/' })

// REQUEST ROUTE
module.exports = function(app) {
  app.get(
    '/api/request',
    RequestController.FetchRequest,
    CommonController.ErrorHandler
  )

  app.post(
    '/api/request',
    RequestController.CreateRequest,
    CommonController.ErrorHandler
  )

  app.post(
    '/api/file',
    uploader.single('file'),
    RequestController.FileUploaderCallback,
    CommonController.ErrorHandler
  )

  app.put(
    '/api/pin',
    RequestController.RestorePIN,
    CommonController.ErrorHandler
  )
}
