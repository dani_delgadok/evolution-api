// LIBRARIES
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// SET SCHEMA
const RequestSchema = new Schema({
  consecutive: {
    type: String
  },
  fullname: {
    type: String
  },
  email: {
    type: String
  },
  cellphone: {
    type: String
  },
  description: {
    type: String
  },
  status: {
    type: String
  },
  createdDate: {
    type: Date
  },
  pin: {
    type: Number
  },
  pinDate: {
    type: Date
  },
  comments: {
    type: Array
  },
  file: {
    type: String,
  },
  type: {
    type: String
  }
})

// SET MODEL
module.exports = mongoose.model('Request', RequestSchema)
